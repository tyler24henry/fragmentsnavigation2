package com.example.fragmentsnavigation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fragmentsnavigation.R
import com.example.fragmentsnavigation.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    // this was the initial way we implemented onCreateView, but we refactored it, as seen below
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        val binding = FragmentHomeBinding.inflate(inflater, container, false)
//        _binding = binding
//        return binding.root
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        // the view is now bound
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGoToRed.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_redFragment2)
        }
        binding.btnGoToGreen.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_greenFragment)
        }
        binding.btnGoToYellow.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_yellowFragment)
        }
        binding.btnGoToPurple.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_purpleFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // no longer bind the view
        _binding = null
    }
}