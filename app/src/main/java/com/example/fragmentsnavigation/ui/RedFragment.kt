package com.example.fragmentsnavigation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fragmentsnavigation.R
import com.example.fragmentsnavigation.databinding.FragmentRedBinding

class RedFragment : Fragment() {

    private var _binding: FragmentRedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentRedBinding.inflate(inflater, container, false).also {
        // the view is now bound
        _binding = it
    }.root

    override fun onDestroyView() {
        super.onDestroyView()
        // no longer bind the view
        _binding = null
    }
}